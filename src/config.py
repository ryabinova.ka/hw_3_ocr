import typing as tp

import pandas as pd
import torch
from pydantic import BaseModel, validator
from sklearn.model_selection import train_test_split
from torch.nn import CTCLoss
from torch.optim import AdamW, Optimizer
from torch.optim.lr_scheduler import CosineAnnealingLR
from torchtext.vocab import Vocab, build_vocab_from_iterator

from src.const import ACCURACY, IMG_HEIGHT, MAX_IMG_WIDTH, SEED


class Config(BaseModel):
    """Config class."""
    project_name: str = '[OCR]'
    experiment_name: str = '1.0'
    seed: int = SEED
    check: bool = False

    cnn_backbone_name: str = 'mobilenetv2_100'
    optimizer: type(Optimizer) = AdamW
    optimizer_kwargs: tp.Mapping = {
        'lr': 5e-4,
        'weight_decay': 5e-6,
    }
    skip_layers_in_weight_decay = ['BatchNorm2d']
    scheduler: tp.Any = CosineAnnealingLR
    scheduler_kwargs: tp.Mapping = {
        'T_max': 20,
        'eta_min': 5e-6,
    }

    image_height: int = IMG_HEIGHT
    max_image_width: int = MAX_IMG_WIDTH
    n_epochs: int = 400
    batch_size: int = 16
    metric: str = ACCURACY

    labels: tp.List = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ']
    blank: str = '_'
    max_char_length: int = 17
    cutoff_coef: float = 3

    dataset_dirpath: str = 'data/full_dataset'
    csv_filepath: str = 'data/full_barcodes.csv'
    checkpoint_dirpath: str = 'checkpoints'
    traced_model_filename: str = 'traced_model.pth'

    valid_ratio: float = 0.2
    train_df: tp.Optional[pd.DataFrame] = None
    valid_df: tp.Optional[pd.DataFrame] = None
    vocab: tp.Optional[Vocab] = None
    criterion: tp.Optional[torch.nn.Module] = None

    class Config:
        """Class to allowd tp.Any as a type."""
        arbitrary_types_allowed = True

    @validator('vocab', always=True)
    def init_vocab(cls, v, values):
        """Init vocab.

        Args:
            v (tp.Any): value, which are not used
            values (dict): fields with values

        Returns:
            vocab object
        """
        return build_vocab_from_iterator(
            iter(values['labels'] + [values['blank']]),
        )

    def __init__(self, **data):
        """Init function.

        Args:
            **data (dict): fields with values  # noqa: RST210
        """
        super().__init__(**data)

        df = pd.read_csv(self.csv_filepath)
        self.train_df, self.valid_df = train_test_split(
            df,
            shuffle=True,
            random_state=self.seed,
            test_size=self.valid_ratio,
        )

        self.criterion = CTCLoss(
            blank=self.vocab[self.blank],
            zero_infinity=False,
        )
