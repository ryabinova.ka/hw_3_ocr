LOGITS = 'logits'
IMAGES = 'images'
LOSS = 'loss'
TARGETS = 'targets'
TARGET_LENGTHS = 'target_lengths'
TRAIN = 'train'
VALID = 'valid'
ACCURACY = 'accuracy'

IMG_HEIGHT = 100
MAX_IMG_WIDTH = 1024
SEED = 25
MAX_PIXEL_INTENSITY = 255
