import os
import random
import typing as tp

import cv2
import numpy as np
import torch
from clearml import Task
from clearml.logger import Logger

from src.config import Config
from src.const import IMG_HEIGHT, MAX_IMG_WIDTH, MAX_PIXEL_INTENSITY


def add_weight_decay(
    model: torch.nn.Module,
    weight_decay: float = 1e-5,
    skip_list: tp.List[str] = None,
):
    """Apply weight decay only on weights and exclude some layers.

    Args:
        model (torch.nn.Module): model object
        weight_decay (float): regularization constant
        skip_list (tp.List[str]): names of layers to skip (e.g. BatchNorm)

    Returns:
        dict of 2 param groups
    """
    def get_layer_classname(param_name, named_modules):  # noqa: WPS430
        param_name_to_search = param_name.rsplit('.', maxsplit=1)[0]
        return str(type(named_modules.get(param_name_to_search)))

    def check_substr(param_name):  # noqa: WPS430
        return any(substr for substr in skip_list if substr in param_name)

    modules_dict = dict(iter(model.named_modules()))

    decay = []
    no_decay = []

    for name, parameter in model.named_parameters():
        classname = get_layer_classname(name, modules_dict)
        if not parameter.requires_grad:
            continue
        if len(parameter.shape) == 1 or check_substr(classname):
            no_decay.append(parameter)
        else:
            decay.append(parameter)

    return [
        {'params': no_decay, 'weight_decay': 0},
        {'params': decay, 'weight_decay': weight_decay},
    ]


def preprocess_ocr(
    im: np.ndarray,
    img_size: tp.Tuple[int, int],
    cutoff_coef: float,
) -> np.ndarray:
    """Preprocess image as it is done in imagenet.

    Args:
        im (np.ndarray): image
        img_size (int): image_size
        cutoff_coef (float): constant for image cutoff

    Returns:
        preprocessed image
    """
    im = im.astype(np.float32)
    im /= MAX_PIXEL_INTENSITY
    cutoff_size = im.shape[0] - im.shape[0] // cutoff_coef
    im = im[cutoff_size:, :, :]

    image_height, image_width, _ = im.shape
    ratio = img_size[0] / image_height
    new_image_width = int(image_width * ratio)
    new_image_heigth = max(IMG_HEIGHT, int(image_height * ratio))

    im = cv2.resize(im, dsize=(new_image_width, new_image_heigth))
    pad_value = max(0, img_size[1] - im.shape[1])
    im = cv2.copyMakeBorder(im, 0, 0, 0, pad_value, cv2.BORDER_CONSTANT, value=0)
    im = im[:, 0:MAX_IMG_WIDTH, :]

    im = np.transpose(im, (2, 0, 1))
    return im  # noqa: WPS331


def set_global_seed(seed: int) -> None:
    """Set all possible seeds for reproducibility.

    Args:
        seed (int): seed itself
    """
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def worker_init_fn(worker_id: int, initial_seed: int = 42):
    """Fixes bug with identical augmentations.

    More info: https://tanelp.github.io/posts/a-bug-that-plagues-thousands-of-open-source-ml-projects/  # noqa: E501

    Args:
        worker_id (int): worker id
        initial_seed (int): seed
    """
    seed = initial_seed ** 2 + worker_id
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)


def get_logger(config: Config) -> Logger:
    """Create experiment in clearml and connect to it.

    Args:
        config (Config): config object

    Returns:
        logger
    """
    task = Task.init(
        project_name=config.project_name,
        task_name=config.experiment_name,
    )
    task.connect(config.dict())
    return task.get_logger()


def squeeze_prediction(prediction: tp.List[int], blank: int) -> tp.List[int]:
    """Function to squeeze predictions.

    Args:
        prediction (tp.List[int]): single prediction sequence
        blank (int): blank token

    Returns:
        squeezed prediction
    """
    squeezed_line = [prediction[0]]
    for index in range(1, len(prediction)):  # noqa: WPS518
        if squeezed_line[-1] == prediction[index]:
            continue
        else:
            squeezed_line.append(prediction[index])

    return [elem for elem in squeezed_line if elem != blank]


def squeeze_targets(target: tp.List[int], blank: int) -> tp.List[int]:
    """Function to cut off padding of blanks.

    Args:
        target (tp.List[int]): target list
        blank (int): blank token

    Returns:
        target without padding
    """
    return [elem for elem in target if elem != blank]
