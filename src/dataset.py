import os
import typing as tp

import albumentations as alb
import cv2
import numpy as np
import pandas as pd
from torch.utils.data import DataLoader, Dataset

from src.augment import augmentations
from src.config import Config
from src.const import IMAGES, TARGET_LENGTHS, TARGETS
from src.train_utils import preprocess_ocr, worker_init_fn


class ImageDataset(Dataset):
    """Image dataset."""
    def __init__(
        self,
        df: pd.DataFrame,
        config: Config,
        augmentation: tp.Optional[alb.Compose] = None,
        preprocessing: tp.Optional[tp.Callable] = None,
    ):
        """Init function.

        Args:
            df (pd.DataFrame): dataframe with dataset
            config (Config): config object
            augmentation (alb.Compose): augmentations compose
            preprocessing (tp.Callable): preprocessing function
        """
        self.df = df
        self.config = config
        self.augmentations = augmentation
        self.preprocessing = preprocessing

    def __len__(self) -> int:
        """Len function.

        Returns:
            len of a dataset
        """
        return len(self.df)

    def __getitem__(self, index: int) -> tp.Dict:
        """Get item from a dataset.

        Args:
            index (int): row number in dataframe

        Returns:
            Return image and target
        """
        row = self.df.iloc[index]
        image_path = os.path.join(self.config.dataset_dirpath, row['filename'])

        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        if self.augmentations:
            image = self.augmentations(image=image)['image']
        if self.preprocessing:
            image = self.preprocessing(  # noqa: WPS317
                image,
                (self.config.image_height, self.config.max_image_width),
                self.config.cutoff_coef,
            )

        raw_target = row['result']
        target = self.config.vocab(list(raw_target))
        target_length = len(target)
        pad_length = max(0, self.config.max_char_length - len(target))
        target = target + [self.config.vocab[self.config.blank]] * pad_length  # noqa: WPS435
        target = np.reshape(np.array(target), (1, len(target)))

        return {IMAGES: image, TARGETS: target, TARGET_LENGTHS: target_length}


def get_datasets(config: Config) -> tp.Tuple[Dataset, Dataset]:
    """Function for dataset generation.

    Args:
        config (Config): config object

    Returns:
        Return train and valid datasets
    """
    train_dataset = ImageDataset(
        config.train_df,
        config,
        augmentation=augmentations,
        preprocessing=preprocess_ocr,
    )

    valid_dataset = ImageDataset(
        config.valid_df,
        config,
        augmentation=None,
        preprocessing=preprocess_ocr,
    )

    return train_dataset, valid_dataset


def get_dataloaders(config: Config) -> tp.Tuple[DataLoader, DataLoader]:
    """Function for dataloader generation.

    Args:
        config (Config): config object

    Returns:
        Return train and valid dataloaders
    """
    train_dataset, valid_dataset = get_datasets(config)

    train_dataloader = DataLoader(
        train_dataset,
        config.batch_size,
        shuffle=True,
        worker_init_fn=worker_init_fn,
    )
    valid_dataloader = DataLoader(
        valid_dataset,
        config.batch_size,
        shuffle=False,
        worker_init_fn=worker_init_fn,
    )

    return train_dataloader, valid_dataloader
