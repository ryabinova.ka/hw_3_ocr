import timm
import torch
from torch import nn  # noqa: WPS458

from src.config import Config


class Model(nn.Module):
    """Model class."""
    def __init__(self, config: Config):
        """Init function.

        Args:
            config (Config): config object
        """
        super(Model, self).__init__()  # noqa: WPS608

        self.cnn = timm.create_model(
            config.cnn_backbone_name,
            pretrained=True,
            features_only=True,
        )
        self.cnn.global_pool = nn.Identity()
        self.cnn.classifier = nn.Identity()

        hidden_state_size = len(config.vocab) * 20
        self.rnn = nn.GRU(
            input_size=672,
            num_layers=2,
            hidden_size=hidden_state_size,
            batch_first=True,
            bidirectional=True,
        )

        self.head = nn.Linear(2 * hidden_state_size, len(config.vocab))
        self.log_softmax = nn.LogSoftmax(dim=-1)

    def forward(self, batch) -> torch.Tensor:
        """Forward function.

        Args:
            batch (Tensor): images

        Returns:
            logits
        """
        x = self.cnn(batch)[3]
        x = x.view(x.shape[0], x.shape[1] * x.shape[2], x.shape[3])
        x = torch.permute(x, (0, 2, 1))

        x = self.rnn(x)[0]
        x = torch.stack([self.head(x[:, i, :]) for i in range(x.shape[1])], dim=1)

        x = self.log_softmax(x)
        x = torch.permute(x, (1, 0, 2))
        return x
