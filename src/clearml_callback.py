from time import time

from catalyst.core.callback import Callback, CallbackNode
from catalyst.core.runner import IRunner
from clearml import Logger

from src.const import TRAIN, VALID


class ClearMLCallback(Callback):  # noqa: WPS338
    """Callback for clearml logging."""
    def __init__(
        self,
        logger: Logger,
    ):
        """Logger init function.

        Args:
            logger (Logger): logger object
        """
        super().__init__(order=90, node=CallbackNode.master)  # noqa: WPS432
        self.logger = logger
        self.start_time = 0

    def on_loader_start(self, runner: IRunner):
        """When train or valid logger starts.

        Args:
            runner (IRunner): runner object
        """
        self.start_time = time()

    def on_loader_end(self, runner: IRunner):
        """When train or valid logger ends.

        Args:
            runner (IRunner): runner object
        """
        dt = time() - self.start_time

        self._report_scalar(
            'time',
            runner.loader_key,
            dt,
            runner.global_epoch_step,
        )

    def _report_scalar(
        self,
        title: str,
        mode: str,
        local_value: float,
        epoch: int,
    ):
        """Function for reports for plots.

        Args:
            title (str): name of a plot
            mode (str): train or valid
            local_value (float): value to log
            epoch (int): epocn number
        """
        self.logger.report_scalar(
            title=title,
            series=mode,
            value=local_value,
            iteration=epoch,
        )

    def on_epoch_end(self, runner: IRunner):
        """When epoch ends.

        Args:
            runner (IRunner): runner object
        """
        for mode, metric_dict in runner.epoch_metrics.items():
            if mode in {TRAIN, VALID}:
                for key, elem in metric_dict.items():
                    title = f'{mode}_{key}'
                    self._report_scalar(
                        title,
                        mode,
                        elem,
                        runner.global_epoch_step,
                    )
        self.logger.flush()
