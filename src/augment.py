import albumentations as alb

PROBABILITY = 0.8

augmentations = alb.Compose([
    alb.RandomBrightnessContrast(p=PROBABILITY),
    alb.RandomGamma(p=PROBABILITY),
    alb.CLAHE(p=PROBABILITY),
    alb.OneOf(
        [
            alb.Blur(),
            alb.MedianBlur(),
        ],
        p=PROBABILITY,
    ),
    alb.JpegCompression(quality_lower=65, p=PROBABILITY),
    alb.RGBShift(
        r_shift_limit=50,
        g_shift_limit=50,
        b_shift_limit=50,
        p=PROBABILITY,
    ),
    alb.RandomShadow(p=PROBABILITY),
])
