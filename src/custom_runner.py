import typing as tp

import torch
from catalyst import dl, metrics

from src.const import (
    ACCURACY,
    IMAGES,
    LOSS,
    TARGET_LENGTHS,
    TARGETS,
    TRAIN,
    VALID,
)
from src.train_utils import squeeze_prediction, squeeze_targets


class CustomRunner(dl.Runner):  # noqa: WPS338
    """Class for runner."""
    def __init__(self, blank: int, *args, **kwargs):
        """Init function to save black.

        Args:
            blank (int): blank token (e.g. 11)  # noqa: RST210, RST213
            *args (dict): some mandatory arguments
            **kwargs (dict): some more mandatory arguments
        """
        super().__init__(*args, **kwargs)
        self.blank = blank

    def on_loader_start(self, runner):
        """When loader starts.

        Args:
            runner (IRunner): runner object
        """
        super().on_loader_start(runner)
        self.meters = {
            LOSS: metrics.AdditiveValueMetric(compute_on_call=False),
            ACCURACY: metrics.AdditiveValueMetric(compute_on_call=False),
        }

    def on_loader_end(self, runner):
        """When loader ends.

        Args:
            runner (IRunner): runner object
        """
        self.loader_metrics[LOSS] = self.meters[LOSS].compute()[0]
        self.loader_metrics[ACCURACY] = self.meters[ACCURACY].compute()[0]
        super().on_loader_end(runner)

    def _is_predictions_equal_targets(
        self,
        predictions: torch.Tensor,
        targets: torch.Tensor,
    ) -> float:
        """Returns fraction of accurate predictions in batch.

        Args:
            predictions (torch.Tensor): outputs of a model
            targets (torch.Tensor): targets from dataloader

        Returns:
            fraction of accurate predictions
        """
        local_predictions = torch.argmax(torch.squeeze(predictions), dim=-1).T
        local_targets = torch.squeeze(targets, dim=1)

        accuracy = 0
        for index in range(local_predictions.shape[0]):
            prediction = squeeze_prediction(
                local_predictions[index].tolist(),
                self.blank,
            )
            target = squeeze_targets(local_targets[index].tolist(), self.blank)

            if prediction == target:
                accuracy += 1

        return accuracy / local_predictions.shape[0]

    def handle_batch(self, batch: tp.Mapping[str, tp.Any]):
        """Batch handler.

        Args:
            batch (tp.Mapping[str, tp.Any]): batch from dataloader
        """
        logits = self.model(batch[IMAGES])

        input_lengths = torch.Tensor(
            logits.shape[1] * [logits.shape[0]],
        ).type(torch.long)
        loss = self.criterion(
            logits,
            torch.squeeze(batch[TARGETS], dim=1),
            input_lengths=input_lengths,
            target_lengths=batch[TARGET_LENGTHS],
        )

        accuracy = self._is_predictions_equal_targets(logits, batch[TARGETS])

        self.batch_metrics.update({LOSS: loss, ACCURACY: accuracy})

        if self.is_train_loader:
            self.epoch_metrics.update({TRAIN: {LOSS: loss, ACCURACY: accuracy}})
        else:
            self.epoch_metrics.update({VALID: {LOSS: loss, ACCURACY: accuracy}})

        self.meters[LOSS].update(
            self.batch_metrics[LOSS].item(), self.batch_size,
        )
        self.meters[ACCURACY].update(
            self.batch_metrics[ACCURACY], self.batch_size,
        )

        if self.is_train_loader:
            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()
