from collections import OrderedDict

from catalyst import dl
from clearml.logger import Logger

from src.clearml_callback import ClearMLCallback
from src.config import Config
from src.const import SEED, TRAIN, VALID
from src.custom_runner import CustomRunner
from src.dataset import get_dataloaders
from src.models import Model
from src.train_utils import add_weight_decay, get_logger, set_global_seed


def get_callbacks(config: Config, logger: Logger):
    """Catalyst callbacks.

    Args:
        config (Config): config object
        logger (Logger): ClearML logger

    Returns:
        list of callbacks
    """
    return [
        dl.CheckpointCallback(
            logdir=config.checkpoint_dirpath,
            loader_key=VALID,
            metric_key=config.metric,
            minimize=False,
            mode='model',
        ),
        dl.EarlyStoppingCallback(
            patience=20,  # noqa: WPS432
            loader_key=VALID,
            metric_key=config.metric,
            minimize=False,
        ),
        ClearMLCallback(logger),
    ]


def train(config: Config, logger: Logger):
    """Train function.

    Args:
        config (Config): config object
        logger (Logger): ClearML logger
    """
    callbacks = get_callbacks(config, logger)
    train_dataloader, valid_dataloader = get_dataloaders(config)
    model = Model(config)

    params = add_weight_decay(  # noqa: WPS110
        model,
        config.optimizer_kwargs['weight_decay'],
        config.skip_layers_in_weight_decay,
    )
    optimizer = config.optimizer(params=params, **config.optimizer_kwargs)
    scheduler = config.scheduler(optimizer=optimizer, **config.scheduler_kwargs)

    runner = CustomRunner(config.vocab[config.blank])
    runner.train(
        model=model,
        criterion=config.criterion,
        optimizer=optimizer,
        scheduler=scheduler,
        callbacks=callbacks,
        loaders=OrderedDict({TRAIN: train_dataloader, VALID: valid_dataloader}),
        num_epochs=config.n_epochs,
        verbose=True,
        check=config.check,
    )


if __name__ == '__main__':
    set_global_seed(SEED)
    global_config = Config()
    global_logger = get_logger(global_config)
    train(global_config, global_logger)
