import os

import cv2
import pandas as pd
import torch

from src.train_utils import preprocess_ocr

if __name__ == '__main__':
    image_height = 100
    max_image_width = 1024
    resize_coef = 3

    model = torch.jit.load('checkpoints/scripted_model.pth')
    df = pd.read_csv('tests/test_data/dataset.csv')

    for index in range(len(df)):  # noqa: WPS518
        filepath = os.path.join('tests/test_data/dataset', df.iloc[index, 0])
        image = cv2.imread(filepath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        preproc_image = preprocess_ocr(  # noqa: WPS317
            image,
            (image_height, max_image_width),
            resize_coef,
        )
        image_tensor = torch.unsqueeze(torch.tensor(preproc_image), 0)

        with torch.no_grad():
            tokens = model(image_tensor)
        print(f'predicted: "{tokens}" and groundtruth is "{df.iloc[index, 1]}"')  # noqa: WPS237, WPS221, WPS421
