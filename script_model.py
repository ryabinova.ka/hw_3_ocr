import typing as tp

import torch

from src.config import Config
from src.models import Model


class ModelWrapper(torch.nn.Module):
    """Model wrapper class."""
    def __init__(self, model, vocab, blank):
        """Init func.

        Args:
            model (nn.Module): model object
            vocab (Vocab): vocab object
            blank (str): black str (e.g. '_')
        """
        super().__init__()
        self.model = model
        self.vocab = vocab
        self.blank = vocab[blank]

    def forward(self, image):
        """Forward func.

        Args:
            image (Tensor): image tensor

        Returns:
            predicted string
        """
        output = self.model.forward(image)
        tokens = torch.argmax(torch.squeeze(output), dim=1)
        tokens: tp.List[int] = tokens.tolist()
        tokens = self.vocab.lookup_tokens(self._squeeze_prediction(tokens))
        tokens = ''.join(tokens)
        return tokens  # noqa: WPS331

    @torch.jit.export
    def _squeeze_prediction(self, prediction: tp.List[int]) -> tp.List[int]:
        squeezed_line = [prediction[0]]
        for index in range(1, len(prediction)):  # noqa: WPS518
            if squeezed_line[-1] == prediction[index]:
                continue
            else:
                squeezed_line.append(prediction[index])

        processed_line: tp.List[int] = []
        for elem in squeezed_line:
            if elem != self.blank:
                processed_line.append(elem)
        return processed_line


if __name__ == '__main__':
    config = Config()
    global_model = Model(config)
    model_params = torch.load('checkpoints/best_0_8.pth', map_location='cpu')
    global_model.load_state_dict(model_params['model_state_dict'])
    global_model.eval()

    wrapper = ModelWrapper(global_model, config.vocab, config.blank)
    scripted_model = torch.jit.script(wrapper)
    torch.jit.save(scripted_model, 'checkpoints/scripted_model.pth')
