# OCR for barcodes 

[Dashboard available via link](https://app.community.clear.ml/projects/0dd0e48d22ac4767ac4b7f89273e3ba5/experiments/aabf8fdcbbe347a084f9c3061380705c/output/execution)

## Environment setup

```bash
python3 -m venv /path/to/new/virtual/environment
source /path/to/new/virtual/environment/bin/activate
pip install -r requirements.txt
```

## Inference

```bash
dvc pull checkpoints/scripted_model.pth.dvc
PYTHONPATH=. CUDA_VISIBLE_DEVICES=0 python3 predict.py
```

## Training

First download the dataset
```bash
dvc pull data/dataset_mark_ksenia.tar.xz.dvc
```
And then unzip it

Start training
```bash
PYTHONPATH=. python3 train.py
```

## Run tests (optional)

```bash
CUDA_VISIBLE_DEVICES=0 pytest
```
