from src.train_utils import squeeze_prediction


def test_squeeze_prediction(test_config):
    squeezed_line = squeeze_prediction(
        [5,  0,  7,  1, 11, 11, 11, 11, 11, 11, 11, 11, 1, 1, 11, 1, 1, 11,
         11, 11, 11, 11, 11, 11, 2, 2, 2, 2, 2, 11, 11, 11, 11, 11, 11, 11,
         11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11],
        test_config.vocab[test_config.blank],
    )
    assert squeezed_line == [5, 0, 7, 1, 1, 1, 2]

    squeezed_line = squeeze_prediction(
        [5, 0, 7, 5, 1, 1, 2, 8, 0, 1, 2, 4, 4, 2, 9],
        test_config.vocab[test_config.blank],
    )
    assert squeezed_line == [5, 0, 7, 5, 1, 2, 8, 0, 1, 2, 4, 2, 9]
