import numpy as np

from src.const import IMAGES, TARGETS


def test_dataset_sanity(test_dataset, test_config):
    item = test_dataset[0]
    assert item[IMAGES].shape == (3, test_config.image_height, test_config.max_image_width)
    assert isinstance(item[TARGETS], np.ndarray)
    assert item[TARGETS].shape == (1, test_config.max_char_length)


def test_dataset_len(test_dataset):
    assert len(test_dataset) > 0
