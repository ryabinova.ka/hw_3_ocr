import torch
from torch import Tensor
from torch import nn

from src.const import IMAGES, TARGETS, TARGET_LENGTHS


def test_model_creation(test_model):
    assert isinstance(test_model, nn.Module)


def test_inference(test_model, test_dataloader):
    batch = next(iter(test_dataloader))
    output = test_model(batch[IMAGES])
    assert isinstance(output, Tensor)


def test_loss(test_model, test_dataloader, test_config):
    batch = next(iter(test_dataloader))
    output = test_model(batch[IMAGES])
    input_lengths = torch.Tensor(test_config.batch_size * [output.shape[0]]).type(torch.long)
    loss = test_config.criterion(
        output,
        torch.squeeze(batch[TARGETS]),
        input_lengths=input_lengths,
        target_lengths=batch[TARGET_LENGTHS]
    )
    assert isinstance(loss.item(), float)
