import os


def test_datasets(test_config):
    assert len(test_config.train_df) > 0
    assert len(test_config.valid_df) > 0

    whole_dataset_len = len(test_config.train_df) + len(test_config.valid_df)
    valid_ratio = round(len(test_config.valid_df) / whole_dataset_len, 1)
    assert valid_ratio == test_config.valid_ratio


def test_image_dir(test_config):
    assert len(os.listdir(test_config.dataset_dirpath)) > 0


def test_vocab(test_config):
    data = '12 34'
    splitted_data = [x for x in data]
    assert len(test_config.vocab(splitted_data)) == 5
