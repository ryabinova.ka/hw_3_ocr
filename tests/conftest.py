import pytest
from torch.utils.data import DataLoader

from src.config import Config
from src.augment import augmentations
from src.dataset import ImageDataset
from src.models import Model
from src.train_utils import preprocess_ocr, worker_init_fn


@pytest.fixture(scope='session')
def test_config():
    return Config(
        batch_size=2,
        valid_ratio=0.2,
        dataset_dirpath='tests/test_data/dataset',
        csv_filepath='tests/test_data/dataset.csv',
    )


@pytest.fixture(scope='session')
def test_dataset(test_config):
    return ImageDataset(
        test_config.train_df,
        test_config,
        augmentation=augmentations,
        preprocessing=preprocess_ocr,
    )


@pytest.fixture(scope='session')
def test_dataloader(test_dataset, test_config):
    return DataLoader(
        test_dataset,
        test_config.batch_size,
        shuffle=False,
        worker_init_fn=worker_init_fn,
    )


@pytest.fixture(scope='session')
def test_model(test_config):
    return Model(test_config)
