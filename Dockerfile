FROM python:3.8

RUN mkdir /app
WORKDIR /app

RUN apt-get update && apt-get install -y libgl1

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY src src
COPY tests tests
COPY train.py train.py

RUN mkdir data
COPY data/barcodes.csv /app/data/barcodes.csv

ENV PYTHONPATH=.

ENTRYPOINT ["/bin/bash"]
